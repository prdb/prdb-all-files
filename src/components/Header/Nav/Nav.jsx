import React, { Component } from 'react';
import Menu from './Menu/Menu.jsx';

class Nav extends Component {
  constructor() {
    super();

    this.state = {
      isOpen: false,
      animateHiding: false,
      oddClick: false,
    }
  }

  onClickHandler = () => {
    this.setState({
      oddClick: !this.state.oddClick,
    }, this.openMenu);

    const FooterLocation = document.getElementsByClassName('Footer__Location')[0];

    if (!this.state.isOpen) {
      FooterLocation.style.zIndex = "-1";
    } else {
      FooterLocation.style.zIndex = "0";
    }
  }

  openMenu = () => {
    if (this.state.isOpen) {
      this.setState({
        animateHiding: true,
      });
    }

    this.setState({
      isOpen: true,
    });
  }

  hideMenu = () => {
    this.setState({
      isOpen: false,
      animateHiding: false,
    })
  }

  render() {
    const { animateHiding, isOpen, oddClick } = this.state;

    return (
      <nav className="Header__Nav">
        <button 
          id="count"
          className={`hamburger hamburger--spin ${oddClick ? 'is-active' : ''}`}
          type="button"
          onClick={this.onClickHandler}>
            <span className="hamburger-box">
              <span className="hamburger-inner"></span>
            </span>
        </button>
      <Menu animateHiding={animateHiding} isOpen={isOpen} hideMenu={this.hideMenu} />
      </nav>
    );
  }
}

export default Nav;
