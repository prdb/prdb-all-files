const Menu = ({ animateHiding, isOpen = false, hideMenu }) => {
  const hideMenuOnAnimationEnd = (e) => e.animationName === 'fadeOutUp' && hideMenu();

  return (
    <ul
    onAnimationEnd={hideMenuOnAnimationEnd}
    className={
      `Header__Nav__Menu animate__faster animate__animated ${isOpen && !animateHiding ?
          'is-active animate__fadeInDown' : 
          `${animateHiding ? 'is-active' : ''} animate__fadeOutUp`}`}>
    <li><a className="Header__Nav__Menu--link" href="#">Home</a></li>
    <li><a className="Header__Nav__Menu--link" href="#">O nas</a></li>
    <li><a className="Header__Nav__Menu--link" href="#">Oferta</a></li>
    <li><a className="Header__Nav__Menu--link" href="#">Realizacje</a></li>
    <li><a className="Header__Nav__Menu--link" href="#">Praca</a></li>
    <li><a className="Header__Nav__Menu--link" href="#">Kontakt</a></li>
  </ul>
  );
}

export default Menu;
