import React, { Component } from 'react';
import './Header.sass';
import Logo from './Logo/Logo.jsx';
import Nav from './Nav/Nav.jsx';

class Header extends Component {
  render() {
    return(
      <header className="Header">
        <Logo />
        <Nav />
      </header>
    );
  }
}

export default Header;
