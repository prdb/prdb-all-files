import React, { Component } from 'react';
import OfferDetails from './OfferDetails/OfferDetails.jsx';

class Offer extends Component {
	render() {
		return(
			<div className="Main__Offer">
				<div className="Main__Offer__Details__header">
					<h3 className="Main__Offer__Details__header--heading">
						Nasza Oferta
					</h3>
				</div>
				<div className="Main__Offer__Details">
					<OfferDetails />
				</div>				
			</div>
		);
	}
}

export default Offer;
