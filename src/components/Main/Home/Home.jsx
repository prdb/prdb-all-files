import React, { Component } from 'react';
import Description from './Description/Description.jsx';
import Logo from './Logo/Logo.jsx';

class Home extends Component {
	render() { 
		return(
			<div className="Main__Home">
				<Logo />
				<Description />
			</div>
		);
	}
}

export default Home;
