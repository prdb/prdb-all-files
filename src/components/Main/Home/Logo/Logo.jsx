import React, { Component } from 'react';

class Logo extends Component {
	render() { 
		return(
			<div className="Main__Home__Logo">
				<img 
					className="Main__Home__Logo--image"
					src="img/prdb.png" 
					alt="logo firmy prd-b"
				/>
			</div>
		);
	}
}

export default Logo;
