import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown } from '@fortawesome/free-solid-svg-icons'

class Description extends Component {
	render() { 
		return(
			<div className="Main__Home__Description">
				<div className="Main__Home__Description__text-wrapper">
					<p className="Main__Home__Description__text-wrapper--content">
						Lorem, ipsum dolor sit amet consectetur, adipisicing elit. Accusantium modi reiciendis aperiam cumque sit quam voluptate minus consequatur reprehenderit eum facere aut soluta doloribus voluptas porro vero mollitia cum, debitis.
					</p>
				</div>	
				<div className="Main__Home__Description__navigate-wrapper">
					<a className="Main__Home__Description__navigate-wrapper__icon" href="#">
						<FontAwesomeIcon 
							className="
								Main__Home__Description__navigate-wrapper--arrow 
								animate__animated 
								animate__bounce
								animate__slow
								animate__infinite
							"
							icon={faArrowDown} 
						/>
					</a>
				</div>
			</div>
		);
	}
}

// should i move those divs into seperate compontents? (text-wrapper, navigate-wrapper)

export default Description;
