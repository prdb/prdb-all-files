import React, { Component } from 'react';
import Home from './Home/Home.jsx';
import About from './About/About.jsx';
import Offer from './Offer/Offer.jsx';

class Main extends Component {
  render() {
	return(
		<main className="Main">
			<Home />
			<About />
			<Offer />
		</main>
	);
  }
}

export default Main;
