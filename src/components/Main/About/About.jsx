import React, { Component } from 'react';
import AboutDetails from './AboutDetails/AboutDetails.jsx'

class About extends Component {
	render() {
		return(
			<div className="Main__About">
				<div className="Main__About__header">
					<h3 className="Main__About__header--heading">
						O nas
					</h3>
				</div>
				<div className="Main__About__Details">
					<AboutDetails />
				</div>				
			</div>
		);
	}
}

export default About;
	