const Header = () => {
  return (
    <div className="Footer__Contact__Header">
      <h3 className="Footer__Contact__Header--title">
        Kontakt
      </h3>
      <h5 className="Footer__Contact__Header--description">
        Masz pytania? Chcesz się czegoś dowiedzieć? Skontaktuj się z nami:
      </h5>
    </div>
  );
}

export default Header;