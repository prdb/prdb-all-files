import React, { Component } from 'react';
import Header from './Header/Header.jsx';
import Options from './Options/Options.jsx';

class Contact extends Component {
	render() {
		return(
			<div className="Footer__Contact">
				<Header />
				<Options />
			</div>
		)
	}
}

export default Contact;
