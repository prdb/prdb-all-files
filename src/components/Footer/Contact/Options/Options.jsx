const Options = () => {
	return (
    	<div className="Footer__Contact__Options">
    		<ul className="Footer__Contact__Options__list">
    			<li className="Footer__Contact__Options__list--list-item">
    				prdb w ornecie
    			</li>
				<li className="Footer__Contact__Options__list--list-item">
					prdbwornecieadres@email.com
				</li>
				<li className="Footer__Contact__Options__list--list-item">
					+48 123 312 123 
				</li>
				<li className="Footer__Contact__Options__list--list-item">
					+48 312 321 221
				</li>
				<li className="Footer__Contact__Options__list--list-item">
					Orneta przemyslowa 123
				</li>	
    		</ul>
		</div>
	);
}

export default Options;