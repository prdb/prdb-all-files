const Copyright = () => {
	return (
		<div className="Footer__Details__copyright">
			<p>
				Copyrights © 2018 PRD-B Orneta. 
			</p>
			<p>
				Developed by Kacper Marzęcki	
			</p>
		</div>
	);
}

export default Copyright;
