import React, { Component } from 'react';
import CompanyDetails from './CompanyDetails/CompanyDetails.jsx';
import Copyright from './Copyright/Copyright.jsx';

class Details extends Component {
	render() {
		return(
			<div className="Footer__Details">
				<CompanyDetails />
				<Copyright />
			</div>
		)
	}
}

export default Details;
