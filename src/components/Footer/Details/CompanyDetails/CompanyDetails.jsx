const CompanyDetails = () => {
	return (
		<div className="Footer__Details__company-details">
			<p>
				NIP: XXX-XXX-XX-XX
			</p>
			<p>
				REGON: XXXXXXXXX
			</p>
			<p>
				Bank BANK XYZ S.A
			</p>
			<p>
				00 0000 0000 0000 0000 0000 0000
			</p>
		</div>
	);
}

export default CompanyDetails;
