import React, { Component } from 'react';
import Map from './Map/Map.jsx';

class Location extends Component {
	
	render() {
		return(
			<div className="Footer__Location">
				<Map />
			</div>
		)
	}
}

export default Location;
