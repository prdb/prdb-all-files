import { Component } from 'react'
import { MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.webpack.css';
import 'leaflet-defaulticon-compatibility';

class Map extends Component {

	render() {
		const position = [54.11146, 20.15148];

		return (
			<MapContainer center={position} zoom={15} scrollWheelZoom={false}>
				<TileLayer
					attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
					url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
				/>
				<Marker position={position}>
					<Popup>
						<div className="Footer__Location__address-popup">
							<p>Ul. Dworcowa 27, 11-130 Orneta</p>
							<a rel="noreferrer" href="https://goo.gl/maps/i6uQgEzrQ6UVL5ae6" target="_blank">Nawiguj</a>
						</div>
					</Popup>
				</Marker>
			</MapContainer>
		)
	}
}

export default Map;
