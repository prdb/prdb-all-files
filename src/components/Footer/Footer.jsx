import React, { Component } from 'react';
import Contact from './Contact/Contact.jsx';
import Location from './Location/Location.jsx';
import Details from './Details/Details.jsx';

class Footer extends Component {
	
	render() {
		return(
			<footer className="Footer">
				<Contact />
				<Location />
				<Details />
			</footer>
		);
	}
}

export default Footer;
